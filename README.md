# GECMembers

A repository meant to contain a simple list of members of KEDCO's GIS and 
Enumeration Centre.


## Tasks:
* Fork this repository onto your profile
* Clone forked repository to system to have a local copy
* Add and commit a file named `members.md` containing a bullet list of all
  KEDCO staff working out of the GIS & Enumeration Centre. Details to be
  presented as thus:
  
  1. <first_name>, <last_name>, <kedco_email>, <kedco_cug>
  2. <first_name>, <last_name>, <kedco_email>, <kedco_cug>
  
  Where a <kedco_email> or <kedco_cug> is not available use "null", without
  the quotes as show below:
  
  3. <first_name>, <last_name>, <kedco_email>, null
  
  

> Note:
This repository is among a series of repositories used to help colleagues get 
familiar with using distributed version control systems such as git for source
code management.
